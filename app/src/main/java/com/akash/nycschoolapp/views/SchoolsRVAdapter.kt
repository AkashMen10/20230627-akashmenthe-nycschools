package com.akash.nycschoolapp.views

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.akash.nycschoolapp.R
import com.akash.nycschoolapp.model.SchoolEntity
import java.util.*

class SchoolsRVAdapter : RecyclerView.Adapter<SchoolsRVAdapter.ViewHolder>() {
    private lateinit var context: Context
    private val schoolsList: ArrayList<SchoolEntity> = ArrayList<SchoolEntity>()
    var onItemClick: ((SchoolEntity) -> Unit)? = null

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvName: TextView = itemView.findViewById(R.id.tv_name)
        val tvAddress: TextView = itemView.findViewById(R.id.tv_address)
        val tvPhone: TextView = itemView.findViewById(R.id.tv_phone)

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(schoolsList[absoluteAdapterPosition])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val transactionItemView =
            LayoutInflater.from(parent.context).inflate(R.layout.schools_row_items, parent, false)
        context = parent.context
        return ViewHolder(transactionItemView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.apply {
            schoolsList[position].let {
                this.tvName.text = it.schoolName
                this.tvAddress.text = it.address
                this.tvPhone.text = it.phoneNumber
            }
        }
    }

    override fun getItemCount(): Int {
        return schoolsList.size
    }

    fun addSchoolsList(list: List<SchoolEntity>) {
        schoolsList.clear()
        schoolsList.addAll(list)
    }
}


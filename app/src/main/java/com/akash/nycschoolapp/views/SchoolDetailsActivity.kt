package com.akash.nycschoolapp.views

import android.app.AlertDialog
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import com.akash.nycschoolapp.R
import com.akash.nycschoolapp.databinding.ActivitySchoolDetailsBinding
import com.akash.nycschoolapp.model.SchoolEntity
import com.akash.nycschoolapp.viewmodel.SchoolDetailsViewModel
import dagger.android.support.DaggerAppCompatActivity
import java.util.*
import javax.inject.Inject

class SchoolDetailsActivity : DaggerAppCompatActivity() {
    private lateinit var binding: ActivitySchoolDetailsBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val schoolDetailsViewModel: SchoolDetailsViewModel by viewModels {
        viewModelFactory
    }
    private lateinit var schoolEntity: SchoolEntity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySchoolDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        addObservers()
        intent?.getParcelableExtra<SchoolEntity>("entity")?.let {
            schoolEntity = it
        }
        schoolDetailsViewModel.getSchoolDetailsByDbn(schoolEntity.dbn)
    }

    // Initialize the observers
    private fun addObservers() {
        schoolDetailsViewModel.progressBarLiveData.observe(this, {
            binding.progressBarLoading.visibility = it
        })

        schoolDetailsViewModel.schoolSatScoreLiveData.observe(this, {
            binding.tvName.text = it.schoolName
            (this.resources.getString(R.string.math_sat_score) + it.satMathAvgScore).also {
                binding.tvMathSatScore.text = it
            }
            (this.resources.getString(R.string.reading_sat_score) + it.satCriticalReadingAvgScore).also {
                binding.tvCriticalSatScore.text = it
            }
            (this.resources.getString(R.string.writing_score) + it.satWritingAvgScore).also {
                binding.tvWritingSatScore.text = it
            }
            binding.tvOverview.text = schoolEntity.overview
            binding.tvWebsite.text = schoolEntity.website
        })

        schoolDetailsViewModel.noDataLiveData.observe(this, {
            val builder = AlertDialog.Builder(this)
            builder.setTitle(R.string.dialog_title)
            builder.setMessage(R.string.no_records)
            builder.setIcon(android.R.drawable.ic_dialog_alert)
            builder.setPositiveButton("Okay"){ _, _ ->
                finish()
            }
            builder.show()
        })
    }
}
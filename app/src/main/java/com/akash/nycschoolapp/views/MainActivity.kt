package com.akash.nycschoolapp.views

import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.akash.nycschoolapp.databinding.ActivityMainBinding
import com.akash.nycschoolapp.viewmodel.SchoolsViewModel
import dagger.android.support.DaggerAppCompatActivity
import java.util.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var schoolsRVAdapter: SchoolsRVAdapter

    private val schoolsViewModel: SchoolsViewModel by viewModels {
        viewModelFactory
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initializeViews()
        addObservers()
        schoolsViewModel.getSchoolList()
    }

    // Initialize recycler view and its adapter
    private fun initializeViews() {
        schoolsRVAdapter = SchoolsRVAdapter()
        val mLayoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )
        binding.schoolRv.apply {
            layoutManager = mLayoutManager
            itemAnimator = DefaultItemAnimator()
            adapter = schoolsRVAdapter
        }

        // Search school details from db on action of done button.
        binding.etSearchInput.setOnEditorActionListener { view, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                schoolsViewModel.getSchoolDetailsBySearch((view as EditText).text.toString())
            }
            false
        }

        schoolsRVAdapter.onItemClick = {
            val intent = Intent(this,SchoolDetailsActivity::class.java)
            intent.putExtra("entity",it)
            startActivity(intent)
        }
    }

    // Initialize the observers
    private fun addObservers() {
        schoolsViewModel.progressBarLiveData.observe(this, {
            binding.progressBarLoading.visibility = it
        })

        schoolsViewModel.schoolListLiveData.observe(this, {
            schoolsRVAdapter.addSchoolsList(it)
            schoolsRVAdapter.notifyDataSetChanged()
        })
    }
}
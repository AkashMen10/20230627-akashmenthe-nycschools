package com.akash.nycschoolapp.retrofit

import com.akash.nycschoolapp.model.SchoolDetailsResponse
import com.akash.nycschoolapp.model.SchoolResponseModel
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("s3k6-pzi2.json")
    suspend fun getSchoolList(
    ): List<SchoolResponseModel>

    @GET("f9bf-2cp4.json")
    suspend fun getSchoolDetails(
        @Query("dbn") dbn:String): SchoolDetailsResponse
}
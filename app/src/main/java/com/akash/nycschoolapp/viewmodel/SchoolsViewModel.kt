package com.akash.nycschoolapp.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.akash.nycschoolapp.model.SchoolEntity
import com.akash.nycschoolapp.model.SchoolResponseModel
import com.akash.nycschoolapp.repository.AppRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject

class SchoolsViewModel @Inject constructor(private val appRepository: AppRepository) :
    ViewModel() {

    private val _progressBarLiveData = MutableLiveData<Int>()
    val progressBarLiveData: MutableLiveData<Int>
        get() = _progressBarLiveData

    private val _schoolListLiveData =
        MutableLiveData<List<SchoolEntity>>()
    val schoolListLiveData: MutableLiveData<List<SchoolEntity>>
        get() = _schoolListLiveData

    // Fetch school list from api
    fun getSchoolList() {
        _progressBarLiveData.postValue(View.VISIBLE)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = appRepository.getSchoolList()
                addSchoolsDataToDb(response)
                fetchAllSchoolsFromDb()
                withContext(Dispatchers.Main) {
                    _progressBarLiveData.postValue(View.GONE)
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    _progressBarLiveData.postValue(View.GONE)
                }
            }
        }
    }


    // Add schools data into database
    private suspend fun addSchoolsDataToDb(schoolResponseModels: List<SchoolResponseModel>) {
        val schools = ArrayList<SchoolEntity>()
        for (schoolResponseModel in schoolResponseModels) {
            val schoolEntity = SchoolEntity(
                schoolResponseModel.dbn,
                schoolResponseModel.schoolName,
                schoolResponseModel.primaryAddressLine1 + ", " + schoolResponseModel.city + ", " + schoolResponseModel.stateCode,
                schoolResponseModel.phoneNumber,
                schoolResponseModel.website,
                schoolResponseModel.overviewParagraph
            )
            schools.add(schoolEntity)
        }
        appRepository.addSchools(schools)
    }

    //    // fetch schools list from local database
    fun fetchAllSchoolsFromDb() {
        viewModelScope.launch(Dispatchers.IO) {
            val schoolEntities = appRepository.fetchSchoolsData()
            withContext(Dispatchers.Main) {
                _schoolListLiveData.postValue(schoolEntities)
            }
        }
    }

    fun getSchoolDetailsBySearch(searchText: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val searchEntities = appRepository.searchSchoolData("%$searchText%")
            withContext(Dispatchers.Main) {
                _schoolListLiveData.postValue(searchEntities)
            }
        }
    }
}
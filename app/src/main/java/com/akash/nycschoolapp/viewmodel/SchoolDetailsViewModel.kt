package com.akash.nycschoolapp.viewmodel

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.akash.nycschoolapp.model.SchoolSatScore
import com.akash.nycschoolapp.repository.AppRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject

class SchoolDetailsViewModel @Inject constructor(private val appRepository: AppRepository) :
    ViewModel() {

    private val _progressBarLiveData = MutableLiveData<Int>()
    val progressBarLiveData: MutableLiveData<Int>
        get() = _progressBarLiveData

    private val _noDataLiveData = MutableLiveData<Boolean>()
    val noDataLiveData: MutableLiveData<Boolean>
        get() = _noDataLiveData

    private val _schoolSatScoreLiveData =
        MutableLiveData<SchoolSatScore>()
    val schoolSatScoreLiveData: MutableLiveData<SchoolSatScore>
        get() = _schoolSatScoreLiveData

    // Fetch school details from api by dbn
    fun getSchoolDetailsByDbn(dbn: String) {
        _progressBarLiveData.postValue(View.VISIBLE)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = appRepository.getSchoolsDetails(dbn)
                withContext(Dispatchers.Main) {
                    if (response.isNotEmpty())
                        _schoolSatScoreLiveData.postValue(response[0])
                    else
                        _noDataLiveData.postValue(true)

                    _progressBarLiveData.postValue(View.GONE)
                }
            } catch (e: Exception) {
                Log.e("Exception", e.message.toString())
                withContext(Dispatchers.Main) {
                    _progressBarLiveData.postValue(View.GONE)
                }
            }
        }
    }
}
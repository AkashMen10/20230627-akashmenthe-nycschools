package com.akash.nycschoolapp.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.akash.nycschoolapp.viewmodel.SchoolDetailsViewModel
import com.akash.nycschoolapp.viewmodel.SchoolsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SchoolsViewModel::class)
    abstract fun bindSchoolViewModel(schoolsViewModel: SchoolsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SchoolDetailsViewModel::class)
    abstract fun bindSchoolDetailsViewModel(schoolDetailsViewModel: SchoolDetailsViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: BaseViewModelFactory): ViewModelProvider.Factory

}
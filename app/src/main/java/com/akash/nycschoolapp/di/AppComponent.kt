package com.akash.nycschoolapp.di

import com.akash.nycschoolapp.NycSchoolApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, ActivityModule::class, ContextModule::class, ViewModelModule::class, NetworkModule::class])
interface AppComponent : AndroidInjector<NycSchoolApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: NycSchoolApplication): Builder
        fun build(): AppComponent
    }

    override fun inject(application: NycSchoolApplication)
}
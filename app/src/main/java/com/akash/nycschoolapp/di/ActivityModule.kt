package com.akash.nycschoolapp.di


import com.akash.nycschoolapp.views.MainActivity
import com.akash.nycschoolapp.views.SchoolDetailsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
/*
Activity module
Register all the activities in this class for DI.
*/
@Module
interface ActivityModule {
    @ContributesAndroidInjector
    fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    fun contributeSchoolDetailsActivity(): SchoolDetailsActivity
}

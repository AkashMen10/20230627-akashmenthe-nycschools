package com.akash.nycschoolapp.di

import android.content.Context
import com.akash.nycschoolapp.NycSchoolApplication
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class ContextModule {
    @Singleton
    @Binds
    abstract fun providesContext(appInstance: NycSchoolApplication): Context
}
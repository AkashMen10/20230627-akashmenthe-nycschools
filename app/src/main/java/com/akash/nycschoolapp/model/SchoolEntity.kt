package com.akash.nycschoolapp.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "school_entity")
data class SchoolEntity(
    @PrimaryKey
    var dbn: String,
    var schoolName: String? = null,
    var address: String? = null,
    var phoneNumber: String? = null,
    var website : String?=null,
    var overview : String?=null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString().toString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(dbn)
        parcel.writeString(schoolName)
        parcel.writeString(address)
        parcel.writeString(phoneNumber)
        parcel.writeString(website)
        parcel.writeString(overview)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SchoolEntity> {
        override fun createFromParcel(parcel: Parcel): SchoolEntity {
            return SchoolEntity(parcel)
        }

        override fun newArray(size: Int): Array<SchoolEntity?> {
            return arrayOfNulls(size)
        }
    }
}
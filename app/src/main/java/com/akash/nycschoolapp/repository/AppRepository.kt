package com.akash.nycschoolapp.repository

import com.akash.nycschoolapp.db.NycSchoolDatabase
import com.akash.nycschoolapp.model.SchoolDetailsResponse
import com.akash.nycschoolapp.model.SchoolEntity
import com.akash.nycschoolapp.model.SchoolResponseModel
import com.akash.nycschoolapp.model.SchoolSatScore
import com.akash.nycschoolapp.retrofit.ApiService
import javax.inject.Inject

/*
App repository
Handles all the api and db calls.
 */
open class AppRepository @Inject constructor(
    private val apiService: ApiService,
    private val schoolDatabase: NycSchoolDatabase
) {
    // Fetch schools data from api
    suspend fun getSchoolList(): List<SchoolResponseModel> {
        return apiService.getSchoolList()
    }

    // Fetch Sat score data from api by school
    suspend fun getSchoolsDetails(dbn: String): SchoolDetailsResponse {
        return apiService.getSchoolDetails(dbn)
    }

    // Add schools data into local db
    suspend fun addSchools(schoolEntities:  List<SchoolEntity>) {
        schoolDatabase.getSchoolDao().addSchools(schoolEntities)
    }

    // Fetch schools data from local db
    suspend fun fetchSchoolsData(): List<SchoolEntity> =
        schoolDatabase.getSchoolDao().fetchSchoolsData()

    // search schools data from local db
    suspend fun searchSchoolData(query: String): List<SchoolEntity> =
        schoolDatabase.getSchoolDao().search(query)
}
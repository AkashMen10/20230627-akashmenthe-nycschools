package com.akash.nycschoolapp.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.akash.nycschoolapp.model.SchoolEntity

/*
Nyc school app database class.
Invoke and build the database.
*/

@Database(
    entities = [SchoolEntity::class],
    version = 1
)
abstract class NycSchoolDatabase : RoomDatabase() {

    abstract fun getSchoolDao(): SchoolDao

    companion object {
        private const val DB_NAME = "school_db"

        @Volatile
        private var nycSchoolDatabase: NycSchoolDatabase? = null

        operator fun invoke(context: Context) = nycSchoolDatabase ?: synchronized(this) {
            nycSchoolDatabase ?: buildDatabase(context).also {
                nycSchoolDatabase = it
            }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                NycSchoolDatabase::class.java,
                DB_NAME
            ).build()
    }
}

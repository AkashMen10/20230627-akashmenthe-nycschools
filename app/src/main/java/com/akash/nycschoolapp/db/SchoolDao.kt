package com.akash.nycschoolapp.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.akash.nycschoolapp.model.SchoolEntity

/*
Nyc school app dao class for db.
Add school data into db.
Fetch school data from db
*/
@Dao
interface SchoolDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addSchools(entities: List<SchoolEntity>)

    @Query("SELECT * FROM school_entity")
    suspend fun fetchSchoolsData(): List<SchoolEntity>

    @Query("SELECT * FROM school_entity WHERE schoolName LIKE :query")
    suspend fun search(query: String): List<SchoolEntity>
}

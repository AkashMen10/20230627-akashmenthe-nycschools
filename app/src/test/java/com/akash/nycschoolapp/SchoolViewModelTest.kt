package com.akash.nycschoolapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.akash.nycschoolapp.model.SchoolEntity
import com.akash.nycschoolapp.repository.AppRepository
import com.akash.nycschoolapp.viewmodel.SchoolsViewModel
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class SchoolViewModelTest {

    private lateinit var schoolsViewModel: SchoolsViewModel

    @Mock
    lateinit var repository: AppRepository

    @Mock
    lateinit var hasReachedMaxObserver: Observer<List<SchoolEntity>>

    @Rule
    @JvmField
    var instantTaskExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    private var testDispatcher = TestCoroutineDispatcher()

    @ExperimentalCoroutinesApi
    private var testCoroutineScope = TestCoroutineScope()


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        schoolsViewModel = SchoolsViewModel(repository)
        schoolsViewModel.schoolListLiveData.observeForever(hasReachedMaxObserver)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getWeatherDataByCity() {
        val list = ArrayList<SchoolEntity>()
        list.add(
            SchoolEntity(
                "A1234",
                "ABC School",
                "Test Pkwy",
                "998899882",
                "www.school.org",
                "Details of school"
            )
        )
        testCoroutineScope.launch(testDispatcher) {
            whenever(repository.fetchSchoolsData()).thenReturn(list)
            schoolsViewModel.fetchAllSchoolsFromDb()
            assertEquals(true, schoolsViewModel.schoolListLiveData.value)
        }
    }
}
# 20230627-AkashMenthe-NYCSchools

## NYC Schools App

- Implemented NYC School app using CityOfNewYork apis.
- Fetching of all NYC high schools data.
- Fetching of reords of high schools.
- Added search functionality


## Language 
Kotlin

## Architecture 
MVVM

## Library

- Retrofit - Integration for network api calls
- Dagger2 - Dependency Injection library
- Coroutines - For handling the concurrency
- RecyclerView - To show the list of highschools
- Room - Local database storage for offline access
- GSON - Pojo classes
- Mockito and Junit - Writing unit test cases
